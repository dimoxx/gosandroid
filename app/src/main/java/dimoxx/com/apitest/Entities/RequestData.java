package dimoxx.com.apitest.Entities;

import java.io.Serializable;

public class RequestData implements Serializable {
    private String userName;
    private String leftAgeValue;
    private String rightAgeValue;
    private String userCity;
    private boolean onlyOnline;

    public RequestData(String userName, String leftAgeValue, String rightAgeValue,
                       String userCity, boolean onlyOnline) {
        this.userName = userName;
        this.leftAgeValue = leftAgeValue;
        this.rightAgeValue = rightAgeValue;
        this.userCity = userCity;
        this.onlyOnline = onlyOnline;
    }


    public String getUserName() {
        return userName;
    }

    public String getLeftAgeValue() {
        return leftAgeValue;
    }

    public void setLeftAgeValue(String leftAgeValue) {
        this.leftAgeValue = leftAgeValue;
    }

    public String getRightAgeValue() {
        return rightAgeValue;
    }

    public void setRightAgeValue(String rightAgeValue) {
        this.rightAgeValue = rightAgeValue;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public boolean isOnlyOnline() {
        return onlyOnline;
    }

    public void setOnlyOnline(boolean onlyOnline) {
        this.onlyOnline = onlyOnline;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}