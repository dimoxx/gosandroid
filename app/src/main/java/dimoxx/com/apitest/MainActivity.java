package dimoxx.com.apitest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import dimoxx.com.apitest.Entities.RequestData;

public class MainActivity extends AppCompatActivity {

    private EditText userName;
    private EditText leftAgeValue;
    private EditText rightAgeValue;
    private EditText userCity;
    private CheckBox onlyOnline;
    private Button searchPeoples;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userName = findViewById(R.id.user_name);
        leftAgeValue = findViewById(R.id.left_age);
        rightAgeValue = findViewById(R.id.right_age);
        userCity = findViewById(R.id.user_city);
        onlyOnline = findViewById(R.id.only_online);
        searchPeoples = findViewById(R.id.search_peoples);
        setListeners();
    }

    private void setListeners() {
        searchPeoples.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PeoplesActivity.class);
                RequestData data = new RequestData(userName.getText().toString(),
                        leftAgeValue.getText().toString(),
                        rightAgeValue.getText().toString(), userCity.getText().toString(),
                        onlyOnline.isChecked());
                intent.putExtra("data", data);
                startActivity(intent);
            }
        });
    }
}
